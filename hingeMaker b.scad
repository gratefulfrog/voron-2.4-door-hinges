$fn = 100;

////////////// TARGETS

movingBlockOnAxis();
fixedBlockOnAxis();

extrusion();

// FIXED BLOCK

//fb x origin is left side of main block
//fb z origin is the extrusion
//fb y origin is the top of the main bock

fbMainZ           = 18;
fbMainX           = 14.4;
fbMainY           = 25;
fbNibZ            = 5.5;
fbNibX            = 5.8;
fbNibY            = fbNibX;
fbNibOffsetX      = -0.5;
fbHorizontalAxisX = 10.4;
fbHorizontalAxisZ = 14;

// MOVING BLOCK

// mb x orign is far left side
// mb y origin is top
// mb z origin is back, 1mm from extrusion
mbFullX           = 29.4;
mbDoorTabX        = 15;
mbOuterTabX       = mbFullX - mbDoorTabX;
mbOuterSectionX   = mbOuterTabX -1;
mbFullY           = 47;
mbOuterTabY       = 10;
mbFullZ           = 17;
mbDoorTabZ        = 3; // the 1mm for the tape is in the space from the extursion
mbHorizontalAxisX = 25.4;
mbHorizontalAxisZ = 13;
mbExtrusionSpaceZ = 1;


//echo(mbOuterTabX);
// MOUNTING BOLT

// mounting bolt origin is fb origin
mntBoltX = 2.4;  // 1.6 radius + 0.8 wall thickness
mntBoltY = fbMainY/2.; 
mtHeadZ  = 12;

doorThickenss = 3;
tapeThickness = 1;

nozzleDia = 0.8;
sphereD   = 2;

m3Dia        = 3.2;
m3Length     = 16;
m3HeadDia    = 5.4;
m3HeadLength = 3.2;
mountingBoltDepth = 4;
axisLength   = 50;

//// EXTURSION

extFaceLength = 7;
extSlotLength = 6;
extSlotDepth  = extSlotLength;
extTestLength = 100;
extCorner = [-18,14];

module extrusion(){
  extrusionH = extTestLength;
  pointVec2D = [[0,0],
                [extFaceLength,0],
                [extFaceLength,extSlotDepth],
                [extFaceLength+extSlotLength,extSlotDepth],
                [extFaceLength+extSlotLength,0],
                [2*extFaceLength+extSlotLength,0],
                [2*extFaceLength+extSlotLength,extFaceLength],
                [2*extFaceLength,extFaceLength],
                [2*extFaceLength,extFaceLength+extSlotLength],
                [2*extFaceLength+extSlotLength,extFaceLength+extSlotLength],
                [2*extFaceLength+extSlotLength,2*extFaceLength+extSlotLength],
                [extFaceLength+extSlotLength,2*extFaceLength+extSlotLength],
                [extFaceLength+extSlotLength,2*extFaceLength],
                [extFaceLength,2*extFaceLength],
                [extFaceLength,2*extFaceLength+extSlotLength],
                [0,2*extFaceLength+extSlotLength],
                [0,extFaceLength+extSlotLength],
                [extSlotLength,extFaceLength+extSlotLength],
                [extSlotLength,extFaceLength],
                [0,extFaceLength]];
  //echo(pointVec2D);
  //rotate([-90,0,0])
    //mirror([0,0,1])
      linear_extrude(extrusionH,center=true)
        translate(extCorner)
          polygon(pointVec2D);
}
//extrusion();



//// BOLT ////

module bolt(L){
  z= L + m3HeadLength;
  cylinder(h=L,d=m3Dia);
  translate([0,0,L])
    cylinder(h=m3HeadLength,d=m3HeadDia);
}
//bolt(axisLength);

module verticalAxis(topZ){
  translate([0,0,-axisLength+topZ])
    bolt(axisLength);
}
//verticalAxis(mbFullY/2.);


////// MOVING BLOCK ///

module movingBlockRaw(){
  extrusionH = mbFullY;
  pointVec2D = [[0,-mbDoorTabZ],
                [mbDoorTabX,-mbDoorTabZ],
                [mbDoorTabX,0],
                [mbFullX,0],
                [mbFullX,-mbFullZ],
                [0,-mbFullZ]];
  linear_extrude(extrusionH)
  difference(){
    polygon(pointVec2D);
    translate([mbHorizontalAxisX,-mbHorizontalAxisZ])
      circle(d=m3Dia);
  }
}
//movingBlockRaw();
/*
Min:  0.00, -17.00, 0.00
   Max:  29.40, 0.00, 47.00
   Size: 29.40, 17.00, 47.00
*/
module movingBlockRounded(){
  extrusionH = mbFullY;
  pOf        = sphereD/2.;
  pointVec2D = [[0,-mbDoorTabZ],
                [mbDoorTabX,-mbDoorTabZ],
                [mbDoorTabX,0],
                [mbFullX,0],
                [mbFullX,-mbFullZ],
                [0,-mbFullZ]];
  sphereVec3D_0 = [[pOf,-mbFullZ+pOf,pOf],
                   [mbFullX-pOf,-mbFullZ+pOf,pOf],
                   [pOf,-mbFullZ+pOf,mbFullY-pOf],
                   [mbFullX-pOf,-mbFullZ+pOf,mbFullY-pOf],
                  ];
  
  cylVec3D_0 = [[pOf,-mbDoorTabZ-pOf,pOf],
                [mbFullX-pOf,-mbDoorTabZ-pOf,pOf],
                [pOf,-mbDoorTabZ-pOf,mbFullY-pOf],
                [mbFullX-pOf,-mbDoorTabZ-pOf,mbFullY-pOf]
                ];
  sphereVec3D_1 = [[mbDoorTabX+pOf,-pOf,pOf],
                 [mbFullX-pOf,-pOf,pOf],
                 [mbDoorTabX+pOf,-pOf,mbFullY-pOf],
                 [mbFullX-pOf,-pOf,mbFullY-pOf],
                ];           
   
  cylVec3D_1 = [[mbDoorTabX+pOf,-mbDoorTabZ-pOf,pOf],
                [mbFullX-pOf,-mbDoorTabZ-pOf,pOf],
                [mbDoorTabX+pOf,-mbDoorTabZ-pOf,mbFullY-pOf],
                [mbFullX-pOf,-mbDoorTabZ-pOf,mbFullY-pOf]
                ];
  sphereVec3D_2 = [[mbFullX-pOf,-mbDoorTabZ,pOf],
                   [mbFullX-pOf,-mbDoorTabZ,mbFullY-pOf],
                  ];  
  difference(){
    union(){    
      hull(){
         for (cy=cylVec3D_0)
          translate(cy)
            helperCyl();
        for (s=sphereVec3D_0)
          translate(s)
            sphere(pOf);
        }
      hull(){
        for (cy=cylVec3D_1)
          translate(cy)
            helperCyl();
        for (s=sphereVec3D_1)
          translate(s)
            sphere(pOf);
        }
    }
    linear_extrude(extrusionH)
      translate([mbHorizontalAxisX,-mbHorizontalAxisZ])
        circle(d=m3Dia);
  }
}
//movingBlockRounded();
/*
 Min:  0.00, -17.00, 0.00
   Max:  29.40, -0.00, 47.00
   Size: 29.40, 17.00, 47.00
*/
module helperCyl(){
  pOf = sphereD/2.;
  rotate([-90,0,0])
    cylinder(h=pOf,r=pOf);
}
//helperCyl();

module movingBlockCutterRaw(){
  extrusionH = mbFullZ;
  pointVec2D = [[mbOuterSectionX,-mbOuterTabY],
                [mbFullX,-mbOuterTabY],
                [mbFullX,-mbFullY+mbOuterTabY],
                [mbOuterSectionX,-mbFullY+mbOuterTabY]];
  //echo(pointVec2D);
  rotate([-90,0,0])
    mirror([0,0,1])
      linear_extrude(extrusionH)
        polygon(pointVec2D);
}
//movingBlockCutterRaw();
/*
 Min:  13.40, -17.00, 10.00
   Max:  29.40, 0.00, 37.00
   Size: 16.00, 17.00, 27.00
*/

module movingBlockCutterRounded(){
  extrusionH = mbFullZ;
  pOf        = sphereD/2.;
  circleVec2D = [[mbOuterSectionX+pOf,-mbOuterTabY-pOf],
                 [mbOuterSectionX+pOf,-mbFullY+mbOuterTabY+pOf]];
  squareVec2D = [[mbFullX-pOf,-mbOuterTabY-pOf],
                 [mbFullX-pOf,-mbFullY+mbOuterTabY]];
  rotate([-90,0,0])
    mirror([0,0,1])
      linear_extrude(extrusionH)
        hull(){
          for (c = circleVec2D)
            translate(c)
              circle(d=sphereD);
          for (sq = squareVec2D)
            translate(sq)
              square(pOf);
        }    
}
//movingBlockCutterRounded();
/*
 Min:  13.40, -17.00, 10.00
   Max:  29.40, 0.00, 37.00
   Size: 16.00, 17.00, 27.00
*/

module movingBlockRawFull(){
  difference(){
    movingBlockRaw();
    movingBlockCutterRaw();
  }
}
//movingBlockRawFull();

module movingBlock(){
  difference(){
    movingBlockRounded();
    movingBlockCutterRounded();
  }
}
//movingBlock();

module movingBlockOnAxis(alpha=0,center=true){
  zT = center ? -mbFullY/2. : 0;
  rotate([0,0,alpha])
  translate([-mbHorizontalAxisX,mbHorizontalAxisZ,zT])
    movingBlock();
}
//movingBlockOnAxis();

///////// FIXED BLOCK ////////////////////

module fixedBlockMainRaw(){
  extrusionH = fbMainY;
  pointVec2D = [[0,0],
                [fbMainX,0],
                [fbMainX,-fbMainZ],
                [0,-fbMainZ]];
  linear_extrude(extrusionH)
  difference(){
    polygon(pointVec2D);
    translate([fbHorizontalAxisX,-fbHorizontalAxisZ])
      circle(d=m3Dia);
  }
}
//fixedBlockMainRaw();

module fixedBlockMainRounded(){
  extrusionH = fbMainY;
  pOf        = sphereD/2.;
  pointVec3D = [[0+pOf,0-pOf,0+pOf],
                [fbMainX-pOf,-pOf,+pOf],
                [fbMainX-pOf,-fbMainZ+pOf,+pOf],
                [+pOf,-fbMainZ+pOf,+pOf],
                [0+pOf,0-pOf,fbMainY-pOf],
                [fbMainX-pOf,-pOf,fbMainY-pOf],
                [fbMainX-pOf,-fbMainZ+pOf,fbMainY-pOf],
                [+pOf,-fbMainZ+pOf,fbMainY-pOf]
                ];
  //linear_extrude(extrusionH)
  difference(){
    hull()
      for (p = pointVec3D)
        translate(p)
          sphere(d=sphereD);
    linear_extrude(extrusionH)
      translate([fbHorizontalAxisX,-fbHorizontalAxisZ])
        circle(d=m3Dia);
  }
}
//fixedBlockMainRounded();


module fixedBlockNibRaw(){
  extrusionH = fbNibY;
  pointVec2D = [[fbNibOffsetX,0],
                [fbNibOffsetX+fbNibX,0],
                [fbNibOffsetX+fbNibX,fbNibZ],
                [fbNibOffsetX,fbNibZ]];
  linear_extrude(extrusionH)
    polygon(pointVec2D);
}
//fixedBlockNibRaw();

module fixedBlockNibRounded(){
  //extrusionH = fbNibY;
  pOf        = sphereD/2.;
  pointVec3D = [[fbNibOffsetX+pOf,pOf,pOf],
                [fbNibOffsetX+fbNibX-pOf,pOf,pOf],
                [fbNibOffsetX+fbNibX-pOf,fbNibZ-pOf,pOf],
                [fbNibOffsetX+pOf,fbNibZ-pOf,pOf],
                
                [fbNibOffsetX+pOf,pOf,fbNibY-pOf],
                [fbNibOffsetX+fbNibX-pOf,pOf,fbNibY-pOf],
                [fbNibOffsetX+fbNibX-pOf,fbNibZ-pOf,fbNibY-pOf],
                [fbNibOffsetX+pOf,fbNibZ-pOf,fbNibY-pOf]
                ];
  hull()
  for (p = pointVec3D)
    translate(p)
      sphere(d=sphereD);
}
//fixedBlockNibRounded();

module fixedBlockNibsRounded(){
  //fixedBlockNibRaw();
  fixedBlockNibRounded();
  translate([0,0,-fbNibY+fbMainY])
    //fixedBlockNibRaw();
    fixedBlockNibRounded();
}
//fixedBlockNibsRaw();

module fixedBlockNibbedRounded(){
  //fixedBlockMainRaw();
  fixedBlockMainRounded();
  fixedBlockNibsRounded();
}
//fixedBlockNibbedRounded();

module fixedBlockNibbedHoledRounded(){
  difference(){
    fixedBlockNibbedRounded();
    fixedBlockBoltHole();
  }
}
//fixedBlockNibbedHoledRounded();

module fixedBlockOnAxis(center=true){
  zT = center ? -fbMainY/2. : 0;
  translate([-fbHorizontalAxisX,fbHorizontalAxisZ,zT])
    fixedBlockNibbedHoledRounded();
}
//fixedBlockOnAxis();

module fixedBlockBoltHole(){
  translate([mntBoltX,0,mntBoltY])
    rotate([90,0,0])
      translate([0,0,-mountingBoltDepth]){
        bolt(m3Length);
      translate([0,0,m3Length])
        cylinder(h=fbMainZ+mountingBoltDepth-m3Length,d=m3HeadDia);
      }
}
//fixedBlockBoltHole();