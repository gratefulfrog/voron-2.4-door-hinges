$fn=100;

x= 10;
y=20;
z=30;
sr = 2;

module test0(){
  minkowski(){
  cube([x,y,z]);
  sphere(r=sr);
  }
}

//translate([sr,sr,sr])
//test0();
/*size 14x24x34
     10+4, 20+4, 30+4
*/

module test1(){
minkowski()
{
  cube([x,y,z]);
  cylinder(r=sr,h=1);
}
}
//translate([sr,sr,0])
//test1();
/*
size 14x24x31
     10+4, 20+4, 30+1
*/

/* Conclusion
Minkowski size = X +2xR, Y+2xR, Z+ totalHeight of second object

to realine:
translate([r,r,negExtent])
*/

module minkyCenter(x,y,z,r,ext=0){
  // if extent is true then consider that only R in Z direction is used
  zT = ext ? ext : 2*r;
  rZ = ext ? 0 : r;
  echo(ext);
  translate([-(x+2*r)/2,-(y+2*r)/2,-(z+zT)/2])
    translate([r,r,rZ])
      children();
}
minkyCenter(x,y,z,2,)
  test0();
