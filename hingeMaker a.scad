$fn = 100;

doorThickenss = 3;
tapeThickness = 1;

fixedPartOriginalStlFileName  = "STL/door_hinge fixed part w axis hole.stl";
movingPartOriginalStlFileName = "STL/door_hinge door part w axis hole.stl";

fixedXt = 44.4305;
fixedYt = -57.32;

movingX = -14.1583;
movingY = -(3.69045 + 50.9568);

extrusionExtWidth = 7;

pinDepth = 5.5; // <- changed!!
pinWidth = 5.8;

fixedLength = 25; //25.6;
fixedWidth  = 12.9;

movingLegth          = 40;
movingWidth          = 26.6;
movingPlateThickness = 3;

nozzleDia = 0.8;
sphereD   = 2;

m3Dia        = 3.2;
m3HeadDia    = 5.4;
m3HeadLength = 3.2;

minDoorX = -(m3Dia/2+nozzleDia);

horizontalAxisPosition = [8,0,-14];

fixedX = 12 +m3Dia/2+nozzleDia -sphereD;
fixedY = fixedLength-sphereD;
fixedZ = 19-sphereD;

movingPartXOverlap   = 15;
movingPartYExtension = 10; // on each side! 
movingPartGap        = 1;

module doorInPosition(){
  x = 100;
  y = x;
  z = doorThickenss + tapeThickness;
  translate([-x+minDoorX,-y/2,-z])
    cube([x,y,z]);
}
//doorInPosition();

module horizontalAxis(){
  translate(horizontalAxisPosition)
    rotate([90,0,0])
      cylinder(h=50,d=m3Dia, center = true);
}
//horizontalAxis();

module fixedNib(){
  z = pinDepth-sphereD/2.;
  y = pinWidth-sphereD;
  x = y;
  translate([(-pinWidth+sphereD)/2,(-pinWidth+sphereD)/2,sphereD/2])
  minkowski(){
    cube([x,y,z]);
    sphere(d=sphereD);
  }
}
//fixedNib();

module fixedNibs(){
  yTrans = (fixedLength -pinWidth)/2;
  vec = [yTrans,-yTrans];
  for (yt = vec)
    translate([0,yt,0])
      fixedNib();
}
//projection()
//fixedNibs();

module boltHoleColumn(){
  xTrans = 0; //pinWidth/2+nozzleDia-m3Dia;
  cl = 30;
  translate([-xTrans,0,-cl/2])
    cylinder(h=cl,d=m3Dia);
}
//boltHoleColumn();

module fixedPlateRaw(){
  x = fixedX;
  y = fixedY;
  z = fixedZ;
  translate([sphereD/2-m3Dia/2-nozzleDia,-y/2,-z])
    minkowski(){
      cube([x,y,z]);
      sphere(d=sphereD);
    }
}
//fixedPlateRaw();

module movingPartRaw(){
  x = fixedX + movingPartXOverlap+movingPartGap;
  y = fixedY + 2*(movingPartYExtension + movingPartGap);
  z = fixedZ;
  translate([fixedX+sphereD/2-m3Dia/2-nozzleDia,-y/2,-z])
  mirror([1,0,0])
    minkowski(){
      cube([x,y,z]);
      sphere(d=sphereD);
    }
}
//mirror([1,0,0])
difference(){
  movingPartRaw();
  fixedPlateRaw();
}

module fixedPlateHoleCutter(){
  x = fixedX+nozzleDia;
  y = fixedY+nozzleDia;
  z = fixedZ;
  translate([sphereD/2-m3Dia/2-2*nozzleDia,-y/2,-z])
    minkowski(){
      cube([x,y,z]);
      sphere(d=sphereD);
    }
}
module testRoundVersion(){
  difference(){
    mobilePlateRound();
    hull()
      difference(){
        fixedPlateHoleCutter();
        fixedPlateRaw();
      }
  }
}

module fixedPlate(){
  difference(){
    union(){
      fixedNibs();
      fixedPlateRaw();
    }
    boltHoleColumn();
    horizontalAxis();
  }
}
//projection()
//fixedPlate();

module mobilePlateRoundRaw(){
  D = fixedLength*2;
  cylinder(h=19-2,d=D);
}

module mobilePlateRound(){
  //scale([1,0.8,1])
  difference(){
    translate([horizontalAxisPosition[0],0,-19+2])
      mobilePlateRoundRaw();
    doorInPosition();
    fixedPlate();
    boltHoleColumn();
      horizontalAxis();
    }
}
//mobilePlateRound();

module simulate(alpha){
  union(){
  translate(-1*horizontalAxisPosition)
    fixedPlate();
  rotate([0,-alpha,0])
    translate(-1*horizontalAxisPosition)
      doorInPosition();
  }
}
 //translate(-1*horizontalAxisPosition*$t/$t)
   // fixedPlate();
//simulate(0*$t);

///////////////////// old stuff importers ///////////////////////

module getFixedSTL(){
  translate([-fixedXt,-fixedYt,0])
  translate([-100,-110,0])
    import(fixedPartOriginalStlFileName);
}
//projection()
//rotate([90,0,0])
//getFixedSTL();

module getmovingSTL(){
  translate([-movingX,-movingY,0])
  translate([-100,-110,0])
    import(movingPartOriginalStlFileName);
}
//projection()
//rotate([90,0,0])
//getmovingSTL();



module hole(){
  translate([0,0,3])
    rotate([0,90,0])
      cylinder(50,1.7,1.7);
}

module hinge(){
  difference(){
    getSTL();
    hole();
  }
}
//hinge();